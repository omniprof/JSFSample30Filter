package com.kfwebstandard.filterlogin.beans;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Simple login bean.
 *
 * @author itcuties http://www.itcuties.com/j2ee/jsf-2-login-filter-example/
 *
 * Modified to use faces-config.xml navigation by Ken Fogel
 */
@Named
@SessionScoped
public class LoginBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(LoginBean.class.getName());

    // Simple user database :)
    private static final String[] users = {"ken:moose", "anna:qazwsx",
        "kate:123456"};

    private String username;
    private String password;

    private boolean loggedIn;

    @Inject
    private NavigationBean navigationBean;

    /**
     * Login operation.
     *
     * @return
     */
    public String doLogin() {
        // Get every user from our sample database :)
        for (String user : users) {
            String dbUsername = user.split(":")[0];
            String dbPassword = user.split(":")[1];

            // Successful login
            if (dbUsername.equals(username) && dbPassword.equals(password)) {
                LOG.info("Successful login");
                loggedIn = true;
                //return navigationBean.toWelcome();
                return "toWelcome";
            }
        }
        LOG.info("Unsuccessful login");

        // Set login ERROR
        FacesMessage msg = new FacesMessage("Really, the user is ken and the password is moose.", "ERROR MSG");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, msg);

        // To to login page
        //return navigationBean.toLogin();
        return "toLogin";

    }

    /**
     * Logout operation.
     *
     * @return
     */
    public String doLogout() {
        // Set the parameter indicating that user is logged in to false
        loggedIn = false;

        // Set logout message
        FacesMessage msg = new FacesMessage("Logout success!", "INFO MSG");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage("errors", msg);

        LOG.info("Successful logout");
        // return navigationBean.redirectToLogin();
        return "redirectToLogin";
    }

    // ------------------------------
    // Getters & Setters
    // ------------------------------
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
}
